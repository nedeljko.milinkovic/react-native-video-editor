
package com.reactlibrary;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.Callback;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.IOException;

public class RNVideoEditorModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public RNVideoEditorModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "RNVideoEditor";
  }

  @ReactMethod
  public void getSample(Promise promise) {
     promise.resolve("SPOIQ");
    Mat img = null;
    try {
      img = Utils.loadResource(reactContext, R.drawable.ic_launcher_background, CvType.CV_8UC4);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}